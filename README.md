
# Yuwen's Personal Zola Page 
This repo is the personal page for IDS721 individual project 1: building a static website using Zola theme deployed on Netlify.

The theme I am using is my customized [Adidoks](https://github.com/Candice1121/adidoks).

Zola deployment Guide: https://www.getzola.org/documentation/deployment/netlify/


## Demo

Video Demo Link: https://youtu.be/PpWvWBx1WmU

Live Netlify URL Link: https://yuwen-ids721-project1.netlify.app/


Live GitLab URL Link: https://yuwen-ids721-zolapage-candice1121-a45a7aea243f53536e4c19dca3c98.gitlab.io/

## Customization
Change the deafult theme from purplrish to matcha green style


| Color             | Hex                                                                |
| ----------------- | ------------------------------------------------------------------ |
| Matcha Green | ![#8ba888](https://via.placeholder.com/10/8ba888?text=+) #8ba888 |


## Deployment:
#### Step 1: Customize Zola theme
Use any zola theme from the official website and fork/template your own customized theme

#### Step 2: Create a new Zola site
Create a repo on Gitlab and follow the instruction on how to  [Deploy Zola on Netlify](https://www.getzola.org/documentation/deployment/netlify/)

#### Step 3: Configuration
Copy configuration file and content directory into your own repo and modify them to your own settings.

1. create a netlify.toml and configure it
    ```
    [build]
    # This assumes that the Zola site is in a docs folder. If it isn't, you don't need
    # to have a `base` variable but you do need the `publish` and `command` variables.
    publish = "/public"
    command = "zola build"

    [build.environment]
    # Set the version name that you want to use and Netlify will automatically use it.
    ZOLA_VERSION = "0.18.0"

    [context.deploy-preview]
    command = "zola build --base-url $DEPLOY_PRIME_URL"
    ```
2. Configure the base url in config.toml
    ```
    # The URL the site will be built for
    base_url = "https://yuwen-ids721-project1.netlify.app/"
    ```

#### Step 4: Deploy on Netlify
Create an account on Netlify and deploy your project linked with GitLab repository.
![Netlify Deployment](imgs/netlify_deploys.png)

Custom your domain url.


## Page Screenshot
### Main page for personal website
![Zola Page](imgs/main_page.png)

### Blog Page
![Blog Page](imgs/blog_page.png)

### Sample Blog
![Sample Blog](imgs/prev.png)
