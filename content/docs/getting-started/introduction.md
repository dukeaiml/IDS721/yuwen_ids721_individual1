+++
title = "Introduction"
description = "This is a personal page using theme AdiDok and deployed using GitLab."
date = 2021-05-01T08:00:00+00:00
updated = 2021-05-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = 'AdiDoks is a Zola theme.'
toc = true
top = false
+++

## Quick Start

One page summary of how to use this page. [Quick Start →](../quick-start/)


## Contributing

Find out how to contribute to Doks. [Contributing →](../../contributing/how-to-contribute/)

## Help

Get help on Doks. [Help →](../../help/faq/)
