+++
title = "Hello, Welcome"
description = "Yuwen's Page Introduction, a netlify deployment page from GitLab"
date = 2021-05-01T09:19:42+00:00
updated = 2021-05-01T09:19:42+00:00
draft = false
template = "blog/page.html"

[taxonomies]
authors = ["YuwenCai"]

[extra]
lead = "This is the first individual project for IDS 721."
+++

