+++
title = "Previous Mini Project"
description = "Sample mini projects showcasing AWS Intergration."
date = 2021-04-20T09:19:42+00:00
updated = 2021-04-20T09:19:42+00:00
draft = false
template = "blog/page.html"

[taxonomies]
authors = ["YuwenC"]

[extra]
lead = "This page links to previous GitLab repos"
+++

## GitLab Links

mini project6: ```https://gitlab.com/Candice1121/yuwen_ids721_min6```

mini project5: ```https://gitlab.com/Candice1121/yuwen_ids721_min5```

mini project4: ```https://gitlab.com/Candice1121/yuwen_ids721_min4```

mini project3: ```https://gitlab.com/Candice1121/yuwen_ids721_min3```