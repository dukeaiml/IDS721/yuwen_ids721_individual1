+++
title = "Yuwen's Personal Page"


# The homepage contents
[extra]
lead = '<b>Project Page for Duke IDS721.</b>'
url = "/docs/getting-started/introduction/"
url_button = "Get started"
repo_version = "Adidoks v0.1.0"
repo_license = "Zola Theme"
repo_url = "https://github.com/Candice1121/adidoks"

# Menu items
[[extra.menu.main]]
name = "Projects"
section = "docs"
url = "/docs/getting-started/introduction/"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20


[[extra.list]]
title = "Creative Content ⚡️"
content = 'Creative Projects for Data in Cloud Scale'

[[extra.list]]
title = "User-friendly Interface"
content = "Use balanced design for data visualization, organization and management."


[[extra.list]]
title = "Dark mode"
content = "Switch to a low-light UI with the click of a button. Change colors with variables to match your branding."

+++
